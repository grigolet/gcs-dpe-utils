from gcsdpeutils import PlcParser
import pytest
import logging
import pandas as pd
import pandas.api.types as ptypes
import pandas.testing as ptesting

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

@pytest.fixture
def plc_parser():
    return PlcParser('tests/files/test_AI.txt')


@pytest.fixture
def second_plc_parser():
    return PlcParser('tests/files/test_AIR.txt')


def test_parse_block_elements(plc_parser):
    elements = plc_parser._parse_block_elements(plc_parser.filepath)
    assert len(elements) == 157


def test_df(plc_parser):
    assert isinstance(plc_parser.df, pd.DataFrame)


def test_name_column(plc_parser):
    assert 'name' in plc_parser.df.columns
    names = plc_parser.df['name']
    assert all(names.str.contains('.PosSt'))


def test_add_two_parsers(plc_parser, second_plc_parser):
    tot_parser = plc_parser + second_plc_parser
    second_tot_parser = second_plc_parser + plc_parser
    assert tot_parser.df.shape == second_tot_parser.df.shape
    with pytest.raises(TypeError):
        failed_parser = plc_parser + None
    with pytest.raises(TypeError):
        failed_parser = plc_parser + pd.DataFrame()
