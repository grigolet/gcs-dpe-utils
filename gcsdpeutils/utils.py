import gcsdpeutils as gdu
from pathlib import Path
import pandas as pd
import numpy as np
import logging


def save_to_excel(df, filepath, sheet_name='Sheet1', mode='w+'):
    """Save a dataframe to an excel file (.xlsx). It uses pandas
    ExcelWriter with the engine 'openpyxl' to write the file.

    Parameters:
        df pd.DataFrame: the pandas dataframe
        filepath str or pathlib.Path: the pandas DataFrame to save
        sheet_name str: the sheet_name where to save the df
        mode str: the mode used to write to the excel file.
    """
    with pd.ExcelWriter(filepath, mode=mode, engine='openpyxl') as writer:
        # pylint: disable=abstract-class-instantiate
        df.to_excel(writer, sheet_name=sheet_name, index=False)
        logging.info(f'Save in {filepath} with sheet name {sheet_name}')


def check_different_smoothing_archiving_deadbands(df, smoothing_column='Driver smoothing value', archiving_column='Archive Deadband Value',
                                      smoothing_type_column='Driver smoothing type', archiving_type_column='Archive smoothing type',
                                     exclude_type_combinations=[(0, 8), (0, 9), (7, 2)]):
    """Check if there are devices where the deadband driver level is bigger than the deadband at archive level.
    The check is done on a pandas dataframe by checking the proper dataframe columns that can be passed
    as arguments. An optional list of couples can be passed to exclude particular combinations of device types
    from the list of devices to check. For example (0, 8) means (Value-dependent smoothing, Smoothing time-dependent AND with relative deadband values)
        
    """
    filters = [((df[smoothing_type_column] == first) &
               (df[archiving_type_column] == second)).values for (first, second) in exclude_type_combinations]
    # Create a logical or between the different combinations. This is equivalent
    # to do np.logical_or.reduce(filters).
    # Ex: a = [True, False, False]
    #     b = [False, False, False] 
    #     c = [False, False, False]
    #           |       |     |
    #         True    False   False
    to_exclude = pd.Series([any(x) for x in list(zip(*filters))])
    filtered = df[~to_exclude]
    smoothing_filter = filtered[smoothing_column] > filtered[archiving_column]
    filtered = filtered[smoothing_filter].reset_index(drop=True)

    return filtered


def check_different_plc_scada_deadbands(df, scada_column='driver_relative_deadband', plc_column='plc_relative_deadband'):
    """Check for misconfigured rows that have driver deadbands smaller that plc deadbands.

    Parameters:
        df pd.DataFrame: the dataframe containing at least the two columsn
        scada_column str: the name of the column of the scada deadabands
        plc_column str: the name of the column of the plc deadbands
    """
    filtered = df[df[scada_column] < df[plc_column]].reset_index(drop=True)

    return filtered


def check_variables_logging(value_counter, points_per_day_threshold=100):
    """Check if there are variables logging too much data. The variables
    should be stored in a ValueCounter object and a threshold of archived points
    per days is used to detect variables logging too much.

    Parameters:
        value_counter gcsdpeutils.ValueCounter: the ValueCounter object containing devices, t1, t2 and their counts
            in the .cache attribute
        points_per_day_threshold int: the number of maximum points per days over which the device should be considered
            as 'misconfigured'
        
    Returns:
        List[str]: list of device names to be checked
    """
    number_of_days = (value_counter.t2 - value_counter.t1).dt.days
    variables_to_be_checked = []
    for variable, count in value_counter.items():
        count_per_day = count / number_of_days
        if count_per_day > points_per_day_threshold:
            variables_to_be_checked.append(variable)

    return variables_to_be_checked
