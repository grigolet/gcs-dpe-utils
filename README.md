# gcs-dpe-utils

Set of utilities to parse and analyse devices configurations from Gas Control Systems. 
This project contains python code to analyse the content of device configuration from
different source (Schneider PLC, WinCC-OA configuration export, CALS info, alarms).

The aim is to provide a set of utils to detect possible mis-configurations of devices.
Few examples of what it can be used for:
* It can detect not-archived device that should be archived or viceversa
* It can detect mis-configuration in time AND/OR deadband configuration
* It can help detect when the smoothing at driver level is higher than the
smoothing at archiving level. 
* It can help check if alarms thresholds are outside the range
of the device.

# Installation

You can install using pip:
```bash
pip install git+https://gitlab.cern.ch/grigolet/gcs-dpe-utils.git
```

# Usage

Look at `example.py` for a complete example.
```python
import gcsdpeutils as gdu

# ScadaParser accepts an exported device fonfig file from winccoa
scada_parser = gdu.ScadaParser('tests/files/test_export_27022020.txt_noArchiveConfigs')
# PlcParser accept the source of code of AI or AIR of the plc
plc_parser = gdu.PlcParser('tests/files/test_AIR.txt')
# Analyser merges the two classes together
analyser = gdu.ScadaPlcAnalyser(scada_parser, plc_parser)

# Analyser contains utilities to detect misconfigured device.
# The results is returned as a dataframe with all the merged
# columns from Plc and Scada parserss
df1 = analyser.check_different_smoothing_archiving_deadbands()
df2 = analyser.check_different_plc_scada_deadbands()

# It is possible to save the dataframes in excel files with custom sheet names.
# Mind the mode used (w+ if file doesn't exist and 'a' to append new sheets to an existing file)
gdu.utils.save_to_excel(df1, 'results.xlsx', sheet_name='DriverDeadbandsBiggerThanArchiveDeadbands', mode='w+')
gdu.utils.save_to_excel(df2, 'results.xlsx', sheet_name='PlcDeadbandsBiggerThanSmoothingDeadbands', mode='a')
```

# Documentation

Each module is documented via docstrings.

## Module `scada.py`

This module contains the class `ScadaParser` used to parse files that are exported
from winccoa (GCS -> Management -> Utilities (local) -> Export CPC device configs).
The Parsers accepts a file as input and a set of parameters with default values
that are used to parse the .txt files into a pandas DataFrame. Once the class
is instantiated the object will contain a `.df` attribute represting the parsed
dataframe from the .txt file.

```python
scada_parser = gdu.ScadaParser('tests/files/test_export_27022020.txt_noArchiveConfigs')
print(scada_parser.df)  # pd.DataFrame
```

The methods of the class are not meant to be used directly, although the .df is public attribute
so one can explore it and perform whatever operation on it.

# Module `plc.py`

This module is similar to `scada.py` in the sense that contains a class called `PlcParser`.
The class `PlcParser` has a constructor accepting a file that should be the exported source
code of the plc software, in particular the definition of __AI__ and __AIR__ devices (see `tests/test_AI.txt`
and `tests/test_AIR.txt` for an example).

```python
plc_parser = gdu.PlcParser('tests/files/test_AIR.txt')
print(plc_parser.df)  # pd.DataFrame
```

The methods of the class are not meant to be used directly, although the .df is public attribute
so one can explore it and perform whatever operation on it.

# Module `analyser.py`

This module contains two classes, `Analyser` and `ValueCounter`. 

Class `ValueCounter` is used to count how many datapoints a particular CPC device is storing
in LHC logging database. It uses pytimber to retrieve statistics on a particular device.
The class can be instantiated with a list of variable names and two time stamps to be used
as an interval over which count the points logged.
Alternatively, it can be used with a static method `ValueCounter.value_counts_variable()`
the performs the operation, provided a pytimber.LoggingDB instance, variable name (as string)
and timestamps.

```python
import pytimber
from gcsdpeutils import ValueCounter
import pandas as pd

ldb = pytimber.LoggingDB()
# We're interested to count how many points are 
# stored for this particular device...
device = 'CMSRPC_DI_PT7404.POSST'
# ...over this particular period
t1, t2 = pd.to_datetime('2020-03-01'), pd.to_datetime('2020-03-10')
counts = ValueCounter.value_counts_variable(ldb, device, t1, t2)
print(counts)
```


Class `Analyser`
is as a container for both `PlcParser` and `ScadaParser` classes. It will provide
the utilities to merge the dataframes of the two classes and run some utility functions
on the merged dataframe to detect possible misconfigurations. The class can be also used only with `PlcParser` alone.

Similarly to `PlcParser` and `ScadaParser`, `Analyser` exposes a public `.df` attribute
that is the merge results of the `.df` of the two plc and scada objects provided. The merge
is an inner merge and the column of the merge can be passed to `Analyser` as arguments
of the constructor (`scada_parser_merge_key` and `plc_parser_merge_key`).
By default, `Analyser` will create a `ValueCounter` object with the list of variables
contained in the `.df` in a particular column. The name of the column is set by passing
in the constructor the `device_name_column` argument (by default is `'Device Name'`).
The `ValueCounter` object is stored under the attribute `.value_counter` and constructor
arguments of `.value_counter` can be passed down directly from constructor arguments of the class `Analyser`.

```python
scada_parser = gdu.ScadaParser('tests/files/test_export_27022020.txt_noArchiveConfigs')
plc_parser = gdu.PlcParser('tests/files/test_AIR.txt')
start = False,
enable_tqdm = True
analyser = gdu.ScadaPlcAnalyser(scada_parser, plc_parser, start=start, enable_tqdm=enable_tqdm)
```