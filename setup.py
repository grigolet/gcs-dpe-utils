import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="gcs-dpe-utils-grigolet", # Replace with your own username
    version="0.0.1",
    author="Gianluca Rigoletti",
    author_email="gianluca.rigoletti@cern.ch",
    description="Set of utilities to parse and analyse devices configurations from Gas Control Systems. ",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.cern.ch/grigolet/gcs-dpe-utils",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['pandas>1', 'pytimber', 'openpyxl'],
    python_requires='>=3.7',
)