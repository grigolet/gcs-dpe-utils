import pandas as pd
from pathlib import Path
import pytimber
from tqdm.auto import tqdm
import gcsdpeutils.utils as ut
from functools import partial
import logging
import reprlib
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - L%(lineno)s - %(message)s')

class ScadaPlcAnalyser:
    """This class is used to merge Scada parsers objects with Plc parser objects.
    It can be useful when the user wants to check deadbands both at plc, driver and archive
    level.
    """
    def __init__(self, scada_parser, plc_parser=None, scada_parser_merge_key='Device Name', plc_parser_merge_key='name', logger=None,
                        device_name_column='Device Name', *args, **kwargs):
        """Initialize the object with a ScadaParser object and an optional PlcParser object.
        The object will have an attribute .df containing a dataframe with the rows for each device.
        If PlcParser object is provided, the .df attribute will be a merge of the .df of ScadaParser
        and .df of PlcParser. The join is of type 'inner' and the keys can be passed as arguments to the class.

        Params:
            scada_parser gcsdpeutils.ScadaParser: the scada parser
            plc_parser gcsdpeutils.PlcParser: the plc parser associated with the scada one
            scada_parser_merge_key str: the Key on ScadaParser .df to be used for merge
            plc_parser_merge_key str: the Key on PlcParser .df to be used for merge
            device_name_column str: the column of .df containing the names of devices. 
                This attribute is used by the value counter object 
        """
        self.scada_parser = scada_parser
        self.plc_parser = plc_parser
        self.scada_parser_merge_key = scada_parser_merge_key
        self.plc_parser_merge_key = plc_parser_merge_key
        self.logger = logger or logging.getLogger(__name__)
        self.device_name_column = device_name_column
        self.df = self._get_total_df()
        self.check_different_smoothing_archiving_deadbands = partial(ut.check_different_smoothing_archiving_deadbands, self.df)
        self.check_different_plc_scada_deadbands = partial(ut.check_different_plc_scada_deadbands, self.df)
        var_list = self.df[self.device_name_column].str.upper().values
        self.value_counter = ValueCounter(var_list, *args, **kwargs)

    def _get_total_df(self):
        """If the attribute of the object plc_parser is defined it will try to merge
        scada parser and plc parser on the provided key and return the resulting df.
        Otherwise it will return the df corresponding to the scada parser.

        Returns:
            pd.DataFrame: the dataframe, either result of a merge or of ScadaParser.df attribute            
        """
        if self.plc_parser:
            df = self._merge_scada_plc_df(self.scada_parser.df, self.plc_parser.df)
            logging.info('Merging scada df and plc df together.')

        else:
            df = self.scada_parser.df
            logging.info('Plc parser is None. Analyser will use only scada parser.')
        return df


    def _merge_scada_plc_df(self, scada_df, plc_df, left_on='Device Name', right_on='name'):
        """Use pandas to merge scada parser's df and plc parser's df. 

        Parameters:
            scada_df pd.DataFrame: the .df of ScadaParser
            plc_df pd.DataFrame: the .df of PlcParser
            left_on str: the key on which to execute the merge of scada_df
            right_on str: they on which to execute the merge of plc_df

        Returns:
            pd.DataFrame: the merged dataframe
        """
        return pd.merge(scada_df, plc_df, left_on=left_on, right_on=right_on)


    def __repr__(self):
        """Represents the object with the representation of the .df of the object"""
        return repr(self.df)
  

class ValueCounter:
    """This class is made for counting the number of points archived for a list of device.
    The user should provide a list of variable names and a period of start and stop.
    The class exposes __getitem__ and items() to access in a dict style the properties
    variable_name -> counts.

    Additionally, the static method 'value_counts_variable' can be used on the class to
    get statistics on a particular variable.
    """
    def __init__(self, var_list=[], t1=None, t2=None, start=True, enable_tqdm=False, *args, **kwargs):
        """Create the objects with a required var_list of device names (ex: ['ATLRPC_DI_PT6104.POSST', 'ATLRPC_DI_PT6204.POSST'])
        This class instantiate a pytimber LoggingDB object for data retrival
        Parameters:
            var_list List[str]: list of string of the device names
            t1 pd.Timestamp: the timestamp from which to start counting archived points
            t2 pd.Timestamp: the timestamp until which to count archived points
            start bool: If True a queries to the logging db will start
            enable_tqdm bool: If True a tqdm wrapper is used to keep count of the queried devices
        """
        if not t1:
            t1 = pd.to_datetime('now') - pd.to_timedelta('30d')
        if not t2:
            t2 = pd.to_datetime('now')
        
        self.var_list = var_list
        self.t1 = t1
        self.t2 = t2
        self.cache = {}
        self.start = start
        self.ldb = pytimber.LoggingDB()
        self.enable_tqdm = enable_tqdm
        if self.start:
            self.count_values()
            

    def count_values(self):
        """This function is used to actually query the database and count the 
        archived points. This function is called automatically if ValueCounter.start = True.
        Otherwhise the users needs to manually call it."""
        if len(self.var_list):
                for var in tqdm(self.var_list, disable=not self.enable_tqdm):
                    key = var.upper()
                    if key in self.cache: continue
                    self.cache[key] = ValueCounter.value_counts_variable(self.ldb, var, self.t1, self.t2)

    
    def __getitem__(self, key):
        """Return the number of archived points for a particular device.
        It will return None if the device is not present, or an int otherwise

        Parameters:
            key str: the device name (ex 'ATLRPC_DI_PT6104.POSST')
        
        Returns:
            int or None: the number of points archived
        """
        return self.cache.get(key)


    def items(self):
        """Accessory method to emulate dict style access to the object.
        items() will access the actual items of self.cache which is an actual dictionary.

        Returns:
           dict_items: the  items of the self.cache 
        """
        return self.cache.items()

    
    def __repr__(self):
        """Represent the object with the main attributes.
        For self.var_list and self.cache reprlib is used to limit
        the size of the resulting string representation.
        """
        var_list_str = reprlib.repr(self.var_list)
        cache_str = reprlib.repr(self.cache)

        return f'ValueCounter(var_list={var_list_str}, t1={self.t1}, t2={self.t2}, cache={cache_str}, start={self.start}, enable_tqdm={self.enable_tqdm})'


    @staticmethod
    def value_counts_variable(ldb, variable, t1, t2):
        """Returns the archived points for a given device in a
        given time range.

        Parameters:
            ldb pytimber.LoggingDB: the instantiated object to be used for queries
            variable str: the device name
            t1 pd.TimeStamp: the start time to count archived points
            t2 pd.Timestamp: the end time to count archived points

        Returns:
            int: the archived points. 0 if device is not present or no points are archived
        """
        variable = variable.upper() 
        data = ldb.getStats(variable, t1, t2)
        if data == {}:
            return 0
        else:
            stats = data[variable]
            value_counts = stats.ValueCount
            return value_counts

