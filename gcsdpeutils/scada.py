import pandas as pd
from pathlib import Path
import logging


class ScadaParser:
    """Parse the data from a file exported
    from scada. The file should be the one
    exported from the menu GCS -> Management 
    -> Utilities (local) -> Export CPC device configs"""
    def __init__(self, filepath=None, skiprows=13, delimiter=';', encoding='latin1', df=None,
                *args, **kwargs):
        """Initialize the object with the path to 
        the exported file"""
        self.filepath = Path(filepath) if filepath else None
        self.skiprows = skiprows
        self.delimiter = delimiter
        self.encoding = encoding
        self.logger = logging.getLogger(__name__)
        if self.filepath:
            self.smoothing_map = self._smoothing_map()
        if df is None:
            self.df = self._parse_df()
            self.df = self._add_procedure_column(self.df)
            self.df = self._add_driver_relative_deadband_column(self.df)
        else:
            self.df = df
        # self.df['device_type'] = self._extract_device_type(self.df['Device Type'])
        # self.df.columns = self.df.columns.str.strip()
        # self.df['driver_smoothing_type_desc'] = self._get_desc_from_map(self.df['Driver smoothing type'], self.smoothing_map())
        
    
    def _smoothing_map(self):
        """Parse the first lines of the file
        containing the map int:str where the 
        integer number is used in the line below
        and the str is a string describing the meaning
        of the archiving/smoothing configuration
        
        Returns:
            maps (dict):the int:str map"""
        maps = {}
        with open(self.filepath, 'r') as f:
            for ix, line in enumerate(f):
                if line[0].isdigit():
                    key = int(line.split(':')[0])
                    value = line[3:].strip()
                    maps[key] = value
                    self.logger.debug('Line: %s. Parsed from map: %s => %s', line, key, value)
                elif line == '\n':
                    continue
                else:
                    break
        return maps

    
    def _parse_df(self):
        """"""
        df = pd.read_csv(self.filepath, skiprows=self.skiprows, delimiter=self.delimiter, encoding=self.encoding)
        df.columns = df.columns.str.strip()
        return df


    def detect_procedure(self, string):
        """Analyse the content of string and returns either
        'time_and_value', 'different' or 'no archive'.
        The following are example of string content:
        'Archive smoothing procedure is defined as Time AND Value: <Device Name>' or
        'Archive smoothing procedure different than Time AND Value' or
        'No archive for: <Device Name>'
        If the first condition is met, procedure = 'time_and_value'
        Otherwise if the second condition is met, procedure = 'different'
        Lastly, procedure = 'no_archive'
        If no condition is met returns 'none'
        """

        if 'Archive smoothing procedure is defined as' in string:
            return 'time_and_value'
        elif 'Archive smoothing procedure differe' in string:
            return 'different'
        elif 'No archive for' in string:
            return 'no_archive'
        else:
            return 'none'


    def _add_procedure_column(self, df):
        """Apply some changes to df to insert new readable columns
        and modify other to remove some useless text or information.

        The following steps are applied:
            * The text column 'Device Type' has the first text part stripped because
              it's repeated all over the lines. The lines can be either in the form:
              'Archive smoothing procedure is defined as Time AND Value: <Device Name>' or
              'Archive smoothing procedure different than Time AND Value' or
              'No archive for: <Device Name>'
              A new column with the label 'procedure' is added.
              If the first condition is met, procedure = 'time_and_value'
              Otherwise if the second condition is met, procedure = 'different'
              Lastly, procedure = 'no_archive'
        """

        df['procedure'] = df['Device Type'].apply(self.detect_procedure).astype('category')
        return df


    def _extract_device_type(self, device_type_serie):
        """Extract device type from a pandas series. It should be 
        associated to the column called 'Device Type'.
        For example, if the string is 'Archive smoothing procedure different than Time AND Value: CPC_AnalogAlarm;'
        it will extract 'CPC_AnalogAlarm'
        Parameters:
            device_type_series (pd.Series): the series of string of associated to the column device type

        Returns:
            pd.Series: the extracted devices from string
        """
        return (device_type_serie.str.split(':', expand=True)
                                 .loc[:, 1]
                                 .str.strip().rename('extracted'))


    def _get_desc_from_map(self, desc_series, smoothing_map):
        """Return the description of the config map given the 
        series containing numbers and the smoothing map where
        for each number a description is present
        Parameters:
            desc_series (pd.Series): the serie of descriptions encoded into integers
            smoothing_map (dict): the smoothing map
        
        Returns:
            pd.Series: the series with the resulting string description
        """
        return desc_series.map(smoothing_map).astype('category')

    
    def _add_driver_relative_deadband_column(self, df):
        """Add a new column called 'driver_relative_deadband' to the dataframe.
        The column is defined as 'Driver smoothing value' / ('RangeMax' - 'Range Min') * 100
        """
        df['driver_relative_deadband'] = df['Driver smoothing value'] / (df['RangeMax'] - df['Range Min']) * 100
        return df


    def __add__(self, other):
        """Add two objects of the same type. This could be useful for example
        to join two files, like noArchive and toReview one since they could 
        refer to the same system.

        The sum will consists in a new object with self.filepath = None and
        self.df a concatenated dataframe along rows.

        The two objects must be of the class ScadaParser
        """
        if not isinstance(other, ScadaParser):
            raise TypeError(f'Objects must be of the same type. Object of type {type(other)} provided instead.')
        
        totdf = self.df.append(other.df)
        return ScadaParser(filepath=None, df=totdf)

