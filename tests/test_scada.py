from gcsdpeutils import ScadaParser
import pytest
import logging
import pandas as pd
import pandas.api.types as ptypes
import pandas.testing as ptesting

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

@pytest.fixture
def scada_parser():
    return ScadaParser('tests/files/test_export_27022020.txt_ArchiveToReview')


@pytest.fixture
def second_scada_parser():
    return ScadaParser('tests/files/test_export_27022020.txt_noArchiveConfigs')


def test_smoothing_map(scada_parser):
    smoothing_map = scada_parser._smoothing_map()
    test_map = {0: 'Value-dependent smoothing',
        1: 'Time-dependent smoothing',
        2: 'Combined smoothing - AND',
        3: 'Combined smoothing - OR',
        4: 'Old/new comparison',
        5: 'Old/new comparison AND time',
        6: 'Old/new comparison OR time',
        7: 'Smoothing with relative deadband values',
        8: 'Smoothing time-dependent AND with relative deadband values',
        9: 'Smoothing time-dependent OR with relative deadband values',
        20: 'No Smoothing Configuration'}
    assert smoothing_map == test_map


@pytest.mark.parametrize(
    'test_input, expected',
    [('No archive for: CPC_AnaDO;ALIAUX_Pr_Spare1.PosSt;Spare AnaDO;%;#.#;0;0;0;0;100;0;0;0', 'no_archive'),
     ('Archive smoothing procedure is defined as Time AND Value: CPC_AnaDO;ALITPC_Pp_PUMP4101.PosSt;Siemens MM420 frequency controller;Hz;##.#;2;1800;0.5;0;50;0;0.5;0', 'time_and_value'),
     ('Archive smoothing procedure different than Time AND Value: CPC_AnalogAlarm;ALIAUX_En_PT0001AA.PosSt;PT0001 - Surface N2 pressure alarm;;###.###;6;10;0;0;0;0;0;0', 'different'),
     ('Random string - Surface N2 pressure alarm;;###.###;6;10;0;0;0;0;0;0', 'none')]
)
def test_detect_procedure(scada_parser, test_input, expected):
    procedure = scada_parser.detect_procedure(test_input)
    assert procedure == expected


def test_parse_df(scada_parser):
    df = scada_parser._parse_df()
    assert df.shape == (7720, 14)
    assert all(df.columns == df.columns.str.strip())


def test_add_procedure_column(scada_parser):
    df = scada_parser._parse_df()
    df = scada_parser._add_procedure_column(df)
    assert 'procedure' in df.columns
    assert ptypes.is_categorical_dtype(df.procedure)


@pytest.mark.parametrize('test_input, expected',
                        [(pd.Series([
                            'Archive smoothing procedure is defined as Time AND Value: CPC_AnalogAlarm',
                            'Archive smoothing procedure different than Time AND Value: CPC_DigitalInput',
                            'No archive for: CPC_Analog'
                        ]), pd.Series([
                            'CPC_AnalogAlarm',
                            'CPC_DigitalInput',
                            'CPC_Analog'
                        ], name='extracted'))])
def test_extract_procedure(scada_parser, test_input, expected):
    result = scada_parser._extract_device_type(test_input)
    ptesting.assert_series_equal(result, expected)



def test_add_two_parsers(scada_parser, second_scada_parser):
    tot_parser = scada_parser + second_scada_parser
    second_tot_parser = second_scada_parser + scada_parser
    assert tot_parser.df.shape == second_tot_parser.df.shape
    with pytest.raises(TypeError):
        failed_parser = scada_parser + None
    with pytest.raises(TypeError):
        failed_parser = scada_parser + pd.DataFrame()


