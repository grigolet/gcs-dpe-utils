import gcsdpeutils as gdu
from pathlib import Path
import pandas as pd
import logging

# Set the level of logging for these modules to critical so 
# they won't log all the messages in case a variable is not found
logging.getLogger('pytimber.pytimber').setLevel(logging.CRITICAL)
logging.getLogger('cmmnbuild_dep_manager').setLevel(logging.CRITICAL)

# Define the main exported files for configs 
SCADA_NOARCHIVE_FILEPATH = Path('tests/files/test_export_27022020.txt_noArchiveConfigs')
SCADA_TOREVIEW_FILEPATH = Path('tests/files/test_export_27022020.txt_ArchiveToReview')
PLC_AI_FILEPATH = Path('tests/files/test_AI.txt')
PLC_AIR_FILEPATH = Path('tests/files/test_AIR.txt')
TIME_START = pd.to_datetime('now') - pd.to_timedelta('1d')
TIME_END = pd.to_datetime('now')

# Create one object for each file
scada_parser_noarchive = gdu.ScadaParser(SCADA_NOARCHIVE_FILEPATH)
scada_parser_toreview = gdu.ScadaParser(SCADA_TOREVIEW_FILEPATH)
plc_parser_ai = gdu.PlcParser(PLC_AI_FILEPATH)
plc_parser_air = gdu.PlcParser(PLC_AIR_FILEPATH)

# It is possible to create a main global ojbect resulting from
# the merge (+ operator) of objects of the same type. This
# applies to ScadaParser and PlcParser
scada_parser = scada_parser_noarchive + scada_parser_toreview
plc_parser = plc_parser_ai + plc_parser_air

# Create an Analyser objects from a ScadaParser and a PlcParser.
# Also it is possible to pass parameters to ValueCounter
analyser = gdu.ScadaPlcAnalyser(scada_parser, plc_parser, t1=TIME_START, t2=TIME_END, start=False, enable_tqdm=True)

# These are the main functions that can be used to analyse
# Get a dataframe with all the rows for which the driver/smoothing deadbands
# are bigger than the archiving deadbands
df1 = analyser.check_different_smoothing_archiving_deadbands()
print('Different smoothing configs:', df1.head())
# Get a dataframe where the plc deadbands are bigger than
# the driver deadbands
df2 = analyser.check_different_plc_scada_deadbands()
print('Different plc scada deadbands:', df2.head())
# Count how many points are logger between TIME_START and
# TIME_END for each of the devices that both PlcParser
# and ScadaParser have in common
analyser.value_counter.count_values()
print('Counts: ', analyser.value_counter.cache)
# Save dataframes to excel with a custom sheet name
gdu.utils.save_to_excel(df1, 'results.xlsx', sheet_name='DriverDeadbandsBiggerThanArchiveDeadbands', mode='w+')
gdu.utils.save_to_excel(df1, 'results.xlsx', sheet_name='PlcDeadbandsBiggerThanSmoothingDeadbands', mode='a')