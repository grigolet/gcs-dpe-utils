import pandas as pd
from pathlib import Path
import logging

class PlcParser:
    def __init__(self, filepath, df=None):
        """Initialize the parser with the filepath of the 
        plc export file. If filepath is provided and the 'df' arg
        is not, a 'df' attribute is created with the
        parsed dataframe. Otherwise, is filepath is None and df
        is provided not parsing will occur. This could happen when the user
        wants to create a PlcParser resulting from the aggregation of two 
        PlcParser associated each one to a single file.
        The dataframe will have one column with
        the name of the device, 2 columns for the range minimum and
        maximum of the device and 2 columns for the range minimum and
        maximum of the adc/dac card.
        
        Parameters:
            filepath (str or pathlib.Path): the path file
            df (pd.DataFrame or None): the DataFrame associated to the parser
        """
        self.filepath = filepath
        self.df = df
        if self.filepath and not df:
            self.df = pd.DataFrame(self._parse_block_elements(self.filepath))
            self.df['name'] = self.df.name.str.strip('_PosSt') + '.PosSt'


    def _parse_block_elements(self, filepath):
        """Parse the main elements (name, min range raw, max range raw and
        plc relative deadband) from the plc exported file.
        The function works by iterating line and considering that the end
        of a device definition is set when the line contains 'PosSt'.

        Parameters:
            filepath (str or pathlib.Path): the path file

        Returns:
            list of dicts: each item is a dict containing the main variables parsed
        """ 
        elements = []
        with open(filepath, 'r') as f:
            for ix, line in enumerate(f):
                if line.startswith('(*') or not line: continue
                if 'PMinRaw' in line:
                    index = line.index('PMinRaw')
                    min_range = int(float(line[index + len('PMinRaw') + 2:].split(',')[0]))
                if 'PMaxRaw' in line:
                    index = line.index('PMaxRaw')
                    max_range = int(float(line[index + len('PMaxRaw') + 2:].split(',')[0]))
                if line.startswith('PDb'):
                    deadband = float(line.split(',')[0].split(':=')[1])
                if line.startswith('PosSt'):
                    posst = line.strip(', \n').split('=>')[1]
                    item = {
                        'min_range_plc': min_range,
                        'max_range_plc': max_range,
                        'plc_relative_deadband': deadband,
                        'name': posst,
                    }
                    elements.append(item)
        return elements


    def __add__(self, other):
        """Add two objects of the same type. This could be useful for example
        to join two files, like <...>_AI.txt and <...>_AIR.txt one since they could 
        refer to the same system.

        The sum will consists in a new object with self.filepath = None and
        self.df a concatenated dataframe along rows.

        The two objects must be of the class ScadaParser
        
        Parameters:
            other gcsdpeutils.PlcParser: the other parser to add
        
        Returns:
            gcsdpeutils.PlcParser: the new parser with .df resulting from conatenation
        """
        if not isinstance(other, PlcParser):
            raise TypeError(
                f'Objects must be of the same type. Object of type {type(other)} provided instead.')

        totdf = self.df.append(other.df)
        return PlcParser(filepath=None, df=totdf)


