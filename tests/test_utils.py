import gcsdpeutils as gdu
import pandas as pd
import pytest


def test_save_to_excel(tmp_path):
    df = pd.DataFrame([
        ['test11', 'test21', 'test31'],
        ['test21', 'test22', 'test33']
    ], columns=['Device Name', 'Device Description', 'Archive Deadband Value'])

    filepath = tmp_path / 'test.xlsx'
    sheet_name = 'Custom Sheet Name'
    gdu.save_to_excel(df, filepath, sheet_name=sheet_name)

    assert filepath.exists()

    dfread = pd.read_excel(filepath, sheet_name=sheet_name, index_col=None)
    assert len(dfread)
    assert dfread.shape == (2, 3)


@pytest.mark.parametrize('test_input, expected', [
    (pd.DataFrame([
        [1, 2, 0, 8],
        [1, 2, 0, 9],
        [1, 2, 7, 2],
        [1, 2, 1, 9],
        [2, 1, 0, 8],
        [2, 1, 0, 9],
        [2, 1, 7, 2],
        [2, 1, 1, 9],
    ], columns=['Driver smoothing value', 'Archive Deadband Value', 'Driver smoothing type', 'Archive smoothing type']),
        pd.DataFrame([
            [2, 1, 1, 9],
        ], columns=['Driver smoothing value', 'Archive Deadband Value', 'Driver smoothing type', 'Archive smoothing type'])),
    (pd.DataFrame([
        [2, 1, 1, 8],
        [2, 1, 2, 9],
        [2, 1, 7, 2], # To exclude because of combination 7, 2 (excluded by default args)
        [1, 2, 3, 9], # To exclude because of combination driver < archive 
    ], columns=['Driver smoothing value', 'Archive Deadband Value', 'Driver smoothing type', 'Archive smoothing type']),
        pd.DataFrame([
            [2, 1, 1, 8],
            [2, 1, 2, 9],
        ], columns=['Driver smoothing value', 'Archive Deadband Value', 'Driver smoothing type', 'Archive smoothing type'])),
])
def test_check_different_smoothing_archiving_deadbands(test_input, expected):
    res = gdu.check_different_smoothing_archiving_deadbands(test_input)
    pd.testing.assert_frame_equal(res, expected)


@pytest.mark.parametrize('test_input, expected', [
    (pd.DataFrame([
        [1, 2],
        [2, 1],
        [2, 2]
    ], columns=['driver_relative_deadband', 'plc_relative_deadband']), 
    pd.DataFrame([
        [1, 2]
    ], columns=['driver_relative_deadband', 'plc_relative_deadband']))])
def test_check_different_plc_scada_deadbands(test_input, expected):
    res = gdu.check_different_plc_scada_deadbands(test_input)
    pd.testing.assert_frame_equal(res, expected)