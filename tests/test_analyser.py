from gcsdpeutils import ValueCounter
from gcsdpeutils import ScadaPlcAnalyser, ScadaParser, PlcParser
import pytest
import pandas as pd


@pytest.mark.parametrize('test_input, expected', [
    ([['CMSDT_PP_PV4003.POSST', 'CMSRPC_DI_PT7405.POSST']], [2, 2]),
    ([['CMSDT_PP_PV4003.POSST', 'CMSRPC_DI_PT7405.POSST'], pd.to_datetime('2020-09-20'), pd.to_datetime('2020-09-21'), False], [0, 0]),
    ([['CMSDT_PP_PV4003.POSST', 'CMSRPC_DI_PT7405.POSST'], pd.to_datetime('2020-09-20'), pd.to_datetime('2020-09-21'), True], [2, 2]),
    ([['ATLRCP_DI_PT6104.POSST', 'ALITPC_DI_PT6104.POSST'], pd.to_datetime('2020-09-20'), pd.to_datetime('2020-09-21'), True], [0, 0]),
])
def test_value_counter(test_input, expected):
    vc = ValueCounter(*test_input)
    if vc.start:
        assert vc.cache != {}
    else:
        assert vc.cache == {}
    for ix, (device_name, counts) in enumerate(vc.items()):
        assert counts >= expected[ix]


def test_value_count_static():
    device = 'CMSRPC_DI_PT7404.POSST'
    t1, t2 = pd.to_datetime('2020-03-01'), pd.to_datetime('2020-03-10')
    import pytimber
    ldb = pytimber.LoggingDB()
    counts = ValueCounter.value_counts_variable(ldb, device, t1, t2)
    assert counts > 0
    device = 'CMSFAKE_DI_PT7404.POSST'
    counts = ValueCounter.value_counts_variable(ldb, device, t1, t2)
    assert counts == 0


def test_analyser():
    scada_parser = ScadaParser(
        'tests/files/test_export_27022020.txt_ArchiveToReview')
    plc_parser = PlcParser('tests/files/test_AI.txt')
    analyser = ScadaPlcAnalyser(scada_parser, plc_parser)
    assert isinstance(analyser.df, pd.DataFrame)
    assert all(analyser.df['name'].isin(plc_parser.df['name']))
    assert isinstance(analyser.value_counter, ValueCounter)

    analyser2 = ScadaPlcAnalyser(scada_parser, plc_parser, t1=pd.to_datetime('now') - pd.to_timedelta('1h'), t2=pd.to_datetime('now'),
                    start=False, enable_tqdm=True)
    
    assert 'Device Name' in  analyser2.df.columns
    assert isinstance(analyser2.value_counter, ValueCounter)
    